#!/usr/bin/python3

#  Copyright (C) 2023, ermi
#
#  Copying and distribution of this file, with or without modification,
#  are permitted in any medium without royalty, provided the copyright
#  notice and this notice are preserved. This file is offered as-is,
#  without any warranty.

import argparse
import socket

def main(addr, port, buffersize):
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.bind((addr, port))
	
	while True:
		payload, client_addr = sock.recvfrom(buffersize)
		num_sent = sock.sendto(payload, client_addr)
		print(client_addr, ': echo' ,len(payload), 'bytes')
		assert(len(payload)==num_sent)

if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-a', '--addr', type=str, default='0.0.0.0')
	parser.add_argument('-p', '--port', type=int, default=7)
	parser.add_argument('-b', '--buffersize', type=int, default=0xffff)
	args = parser.parse_args()
	
	try:
		main(args.addr, args.port, args.buffersize)
	except KeyboardInterrupt:
		pass
